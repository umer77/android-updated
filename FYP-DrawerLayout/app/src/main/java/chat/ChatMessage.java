package chat;

/**
 * Created by Rana Abdullah Shafiq on 5/6/2016.
 */
public class ChatMessage {
    public boolean left;
    public String message;

    public ChatMessage(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
    }
}
