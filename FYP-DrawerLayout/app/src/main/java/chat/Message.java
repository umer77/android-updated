package chat;

/**
 * Created by Rana Abdullah Shafiq on 5/10/2016.
 */
public class Message
{
    public String To_RegID;
    public String To_Name;
    public String From_Name;
    public String From_RegID;
    public String TextMessage;
}
