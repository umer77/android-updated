package chat;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.DataSetObserver;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Rana Abdullah Shafiq on 5/6/2016.
 */
public class ChatBubbleActivity extends AppCompatActivity {
    private static final String TAG = "ChatActivity";

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;
    String json;
    Message m;

    static boolean active = false;

    Intent intent;
    private boolean side = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.activity_chat);

        json = i.getStringExtra("data");
        final Gson gson = new Gson();

        m = gson.fromJson(json,Message.class);


        buttonSend = (Button) findViewById(R.id.buttonSend);

        listView = (ListView) findViewById(R.id.listView1);






        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.chat_singlemsg);
        listView.setAdapter(chatArrayAdapter);

        chatText = (EditText) findViewById(R.id.chatText);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                m.TextMessage = chatText.getText().toString();
                json = gson.toJson(m).toString();
                sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

        if (i.getBooleanExtra("r",false))
        {
            onMessageReceived();
        }

    }

    private void onMessageReceived() {
        Bundle data = getIntent().getExtras();
        m = new Message();
        m.TextMessage = data.getString("TextMessage");

        chatArrayAdapter.add(new ChatMessage(!side,m.TextMessage));
        m.From_RegID = data.getString("To_RegID");
        m.To_RegID = data.getString("From_RegID");
        m.From_Name = data.getString("To_Name");
        m.To_Name = data.getString("From_Name");


    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(android.content.Context context, Intent intent) {
            Bundle data = intent.getExtras();
            m = new Message();
            m.TextMessage = data.getString("TextMessage");
            chatArrayAdapter.add(new ChatMessage(!side,m.TextMessage));



            m.From_RegID = data.getString("To_RegID");
            m.To_RegID = data.getString("From_RegID");
            m.From_Name = data.getString("To_Name");
            m.To_Name = data.getString("From_Name");
        }
    };

    private boolean sendChatMessage(){
        chatArrayAdapter.add(new ChatMessage(side, chatText.getText().toString()));

        DownloadFilesTask d = new DownloadFilesTask();
        d.setJsonString(json);
        d.execute();
        chatText.setText("");
        //side = !side;
        return true;
    }

    class DownloadFilesTask extends AsyncTask<URL, Integer, String> {

        private String json;
        public void setJsonString (String j)
        {
            json = j;
        }

        protected String doInBackground(URL... urls) {
            HttpURLConnection httpcon = null;

            String url = null;
            String data = null;
            String result = null;
            StringBuilder sb = new StringBuilder();
            try {
//Connect
                httpcon = (HttpURLConnection) ((new URL("http://chatserver-1.apphb.com/api/Message/").openConnection()));
                httpcon.setDoOutput(true);
                httpcon.setDoInput(true);
                httpcon.setUseCaches(false);
                httpcon.setConnectTimeout(10000);
                httpcon.setReadTimeout(10000);
                httpcon.setRequestProperty("Content-Type", "application/json");
                httpcon.setRequestProperty("Accept", "application/json");
                httpcon.setRequestMethod("POST");
                httpcon.connect();
                OutputStream os = httpcon.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(json.toString());
                osw.flush();
                osw.close();

                int HttpResult =httpcon.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpcon.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+sb.toString());

                }else{
                    System.out.println(httpcon.getResponseMessage());
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                httpcon.disconnect();
                e.printStackTrace();
            }
            return "Good";
        }
        protected void onPostExecute(String result) {

            Toast.makeText(getApplicationContext(), "Sent", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        active = true;
        registerReceiver(receiver, new IntentFilter("new"));


    }

    @Override
    public void onPause() {
        super.onPause();
        active = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }



}