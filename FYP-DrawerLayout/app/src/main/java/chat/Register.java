package chat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Register extends AppCompatActivity {

    User u;
    String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        u = new User();

        Intent intent = getIntent();
        String t = intent.getStringExtra("t");
        u.Registration_ID = t;

        u.Name = getIntent().getStringExtra("x");

        u.Status = true;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor edit = sp.edit();
        edit.putBoolean("completed",true);
        edit.commit();

        Gson gson = new Gson();
        json = gson.toJson(u);

        DownloadFilesTask a = new DownloadFilesTask();
        a.setJsonString(json);
        a.execute();


        Button b = (Button)findViewById(R.id.button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText)findViewById(R.id.editText);
                //u.Name = et.getText().toString();




            }
        });






    }

    class DownloadFilesTask extends AsyncTask<URL, Integer, String> {

        private String json;
        public void setJsonString (String j)
        {
            json = j;
        }

        protected String doInBackground(URL... urls) {
            HttpURLConnection httpcon = null;

            String url = null;
            String data = null;
            String result = null;
            StringBuilder sb = new StringBuilder();
            try {
//Connect
                httpcon = (HttpURLConnection) ((new URL("http://chatserver-1.apphb.com/api/User/").openConnection()));
                httpcon.setDoOutput(true);
                httpcon.setDoInput(true);
                httpcon.setUseCaches(false);
                httpcon.setConnectTimeout(10000);
                httpcon.setReadTimeout(10000);
                httpcon.setRequestProperty("Content-Type", "application/json");
                httpcon.setRequestProperty("Accept", "application/json");
                httpcon.setRequestMethod("POST");
                httpcon.connect();
                OutputStream os = httpcon.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
                osw.write(json.toString());
                osw.flush();
                osw.close();

                int HttpResult =httpcon.getResponseCode();
                if(HttpResult ==HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            httpcon.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();

                    System.out.println(""+sb.toString());

                }else{
                    System.out.println(httpcon.getResponseMessage());
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                httpcon.disconnect();
                e.printStackTrace();
            }
            return "Good";
        }
        protected void onPostExecute(String result) {
            Intent inten = new Intent(getApplicationContext(), MainActivity.class);
            inten.putExtra("name",u.Name.toString());
            startActivity(inten);

        }
    }

}
