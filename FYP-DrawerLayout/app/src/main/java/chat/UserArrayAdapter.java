package chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by Rana Abdullah Shafiq on 5/10/2016.
 */
public class UserArrayAdapter extends ArrayAdapter {
    private Context context;
    private ArrayList<User> users;

    static class HolderView {
        public TextView Name;
        public TextView Reg;
    }

    @SuppressWarnings("unchecked")
    public UserArrayAdapter(Context c, ArrayList<User> users) {
        super(c, R.layout.single_user_selection, users);
        this.context = c;
        this.users = users;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.single_user_selection, parent, false);

            HolderView listItem = new HolderView();
            listItem.Name = (TextView) convertView.findViewById(R.id.name);
            listItem.Reg = (TextView) convertView.findViewById(R.id.reg);

            convertView.setTag(listItem);
        }

        HolderView listItem = (HolderView) convertView.getTag();

        User s = users.get(position);

        listItem.Name.setText(s.Name);
        //listItem.Reg.setText(s.Registration_ID);

        return convertView;
    }
}
