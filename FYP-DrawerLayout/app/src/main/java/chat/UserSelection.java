package chat;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class UserSelection extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private ListView dynamicList;
    private ArrayList<User> users;
    private UserArrayAdapter dla;
    private String json;
    private String token;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_selection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        users = new ArrayList<User>();

        dla = new UserArrayAdapter(this, users);

        dynamicList = (ListView) findViewById(R.id.users);
        dynamicList.setAdapter(dla);
        dynamicList.setOnItemClickListener(this);

        new DownloadFilesTask().execute();

        token = getIntent().getStringExtra("token");
        name = getIntent().getStringExtra("name");

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        User s = users.get(position);
        chat.Message m = new chat.Message();

        m.From_Name = name;
        m.From_RegID = token;
        m.To_Name = s.Name;
        m.To_RegID = s.Registration_ID;
        m.TextMessage = "Hi";
        Gson gson = new Gson();
        String j = gson.toJson(m);

        //Toast toast = Toast.makeText(this, j.toString() ,Toast.LENGTH_LONG );
        //toast.show();

        Intent i = new Intent(getApplicationContext(),ChatBubbleActivity.class);
        i.putExtra("data",j.toString());
        startActivity(i);

    }

    /*@Override
    public void onClick(View v) {
        Student s = new Student();
        s.name = etName.getText().toString();
        s.dept = etDept.getText().toString();
        s.cgpa = etCGPA.getText().toString();
        students.add(s);
        dla.notifyDataSetChanged();
    }*/
    class DownloadFilesTask extends AsyncTask<URL, Integer, String> {
        protected String doInBackground(URL... urls) {
            HttpURLConnection httpcon = null;

            String url = null;
            String data = null;
            String result = null;
            try {
//Connect
                httpcon = (HttpURLConnection) ((new URL("http://chatserver-1.apphb.com/api/User").openConnection()));
                //httpcon.setDoOutput(true);
                httpcon.setRequestProperty("Content-Type", "application/json");
                httpcon.setRequestProperty("Accept", "application/json");
                httpcon.setRequestMethod("GET");
                httpcon.connect();
                BufferedReader br = new BufferedReader(new InputStreamReader(httpcon.getInputStream(), "UTF-8"));

                String line = null;
                StringBuilder sb = new StringBuilder();

                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }

                br.close();
                result = sb.toString();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                httpcon.disconnect();
                e.printStackTrace();
            }
            return result;
        }
        protected void onPostExecute(String result) {
            json = result;
            //Toast.makeText(getApplicationContext(), json, Toast.LENGTH_LONG).show();

            Gson gson = new Gson();

            Type type = new TypeToken<ArrayList<User>>() {}.getType();
            ArrayList<User> re = gson.fromJson(json, type);

            for(User record : re){
                //if(!record.Registration_ID.equals(token)){
                    users.add(record);
                //}
            }
            dla.notifyDataSetChanged();
        }
    }
}


