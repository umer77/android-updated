package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class PostAdapter extends ArrayAdapter<Post> {


    public PostAdapter(Context context, ArrayList<Post> communities) {
        super(context, R.layout.community_row_layout,communities);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.community_row_layout, parent, false);

        Post comm = getItem(position);

        TextView title = (TextView)view.findViewById(R.id.title);
        title.setText(comm.getDescription());

        TextView tags = (TextView)view.findViewById(R.id.tag);
        tags.setText(comm.getDate());

        TextView count = (TextView)view.findViewById(R.id.ansCount);
        count.setText(Integer.toString(comm.getComments().size()));

        return view;
    }
}
