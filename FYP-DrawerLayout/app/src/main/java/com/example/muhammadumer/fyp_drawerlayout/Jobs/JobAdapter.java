package com.example.muhammadumer.fyp_drawerlayout.Jobs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 2/15/2016.
 */
public class JobAdapter extends ArrayAdapter<Jobs> {


    public JobAdapter(Context context, ArrayList<Jobs> values) {
        super(context, R.layout.job_row_layout, values);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.job_row_layout, parent, false);

        Jobs j = getItem(position);

        TextView title = (TextView)view.findViewById(R.id.jobTitle);

        title.setText(j.getTitle());

        TextView loc = (TextView)view.findViewById(R.id.jobLocation);
        loc.setText(j.getLocation());

        TextView desc = (TextView)view.findViewById(R.id.jobDescription);
        desc.setText(j.getDescription());

        TextView date = (TextView)view.findViewById(R.id.jobDate);
        date.setText(j.getApplyDate());

        ImageView i = (ImageView)view.findViewById(R.id.image);
        i.setImageResource(R.drawable.books);

        return view;
    }
}
