package com.example.muhammadumer.fyp_drawerlayout.Search;

import java.io.Serializable;

/**
 * Created by MuhammadUmer on 5/19/2016.
 */
public class Degree implements Serializable {

    private int degreeId;
    private String degreeName;
    private String degreeType;
    private String universityId;
    private String universityName;

    public int getDegreeId() {
        return degreeId;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public String getDegreeType() {
        return degreeType;
    }

    public String getUniversityId() {
        return universityId;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setDegreeId(int degreeId) {
        this.degreeId = degreeId;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public void setDegreeType(String degreeType) {
        this.degreeType = degreeType;
    }

    public void setUniversityId(String universityId) {
        this.universityId = universityId;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
