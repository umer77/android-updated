package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class PostDetailLayout extends Activity implements View.OnClickListener {

    private TextView title;

    private ListView answersList;
    private ArrayList<Comment> communityAnswers;
    private ArrayAdapter<Comment> communityAnswersArrayAdapter;

    private ImageView answerIcon;
    private ImageView backIcon;
    private Post c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.community_questions_details_layout);

        title = (TextView)findViewById(R.id.title);

        answersList = (ListView)findViewById(R.id.answersList);
        answerIcon = (ImageView)findViewById(R.id.answericon);
        backIcon = (ImageView)findViewById(R.id.backIcon);

        c = (Post)getIntent().getSerializableExtra("Data");

        title.setText(c.getDescription());


        communityAnswers = new ArrayList<>();
        communityAnswers = c.getComments();

        TextView date = (TextView)findViewById(R.id.date);
        date.setText(String.valueOf(c.getDate()));

        communityAnswersArrayAdapter = new CommentAdapter(this,c.getComments());
        answersList.setAdapter(communityAnswersArrayAdapter);

        setListViewHeightBasedOnChildren(answersList);
        answerIcon.setOnClickListener(this);
        backIcon.setOnClickListener(this);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        params.height += 30;
        listView.setLayoutParams(params);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.answericon:
                Intent i = new Intent(this,PostAnswer.class);
                i.putExtra("Data",c);
                startActivity(i);
                this.finish();
                break;
            case R.id.backIcon:
                i = new Intent(this,PostLayout.class);
                startActivity(i);
                break;
        }
    }
}
