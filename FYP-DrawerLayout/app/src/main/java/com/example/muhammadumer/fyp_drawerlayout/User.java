package com.example.muhammadumer.fyp_drawerlayout;

/**
 * Created by MuhammadUmer on 2/21/2016.
 */
public class User {

    private int UserId;
    private String Username;
    private String FirstName;
    private String LastName;
    private String Email;
    private String Password;
    private String Gender;
    private String UserStatus;
    private String Address;

    public void setUserId(int userId) {
        UserId = userId;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public void setPassword(String password) {Password = password;}

    public void setGender(String gender) {
        Gender = gender;
    }

    public void setAddress(String address) {Address = address;}

    public void setUserStatus(String status) {
        UserStatus = status;
    }

    public int getUserId() {
        return UserId;
    }

    public String getUsername() {
        return Username;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getEmail() {
        return Email;
    }

    public String getPassword() {
        return Password;
    }

    public String getGender() {
        return Gender;
    }

    public String getAddress() {
        return Address;
    }

    public String getUserStatus() {
        return UserStatus;
    }
}