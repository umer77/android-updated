package com.example.muhammadumer.fyp_drawerlayout.Models;

/**
 * Created by MuhammadUmer on 5/1/2016.
 */
public interface TaskListener {

    void onTaskStarted();

    void onTaskFinished(String result);
}
