package com.example.muhammadumer.fyp_drawerlayout.Members_Area;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.User;
import com.google.gson.Gson;


/**
 * Created by MuhammadUmer on 2/18/2016.
 */
public class RegisterLayout extends Fragment implements TaskListener, View.OnClickListener, View.OnFocusChangeListener {


    TextView username;
    TextView firstname;
    TextView lastname;
    TextView email;
    TextView password;
    TextView confirmPassword;
    Spinner gender;
    Spinner status;
    TextView address;
    Button register;
    RelativeLayout layout;

    String gendr[] = {"Male","Female"};

    String helpr[] = {"Student","Professional"};

    ArrayAdapter<String> genderAdapter;
    ArrayAdapter<String> statusAdapter;

    ProgressDialog load;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.register_layout,container,false);

        username = (TextView)view.findViewById(R.id.usernameBox);
        firstname = (TextView)view.findViewById(R.id.firstNameBox);
        lastname = (TextView)view.findViewById(R.id.lastNameBox);
        email = (TextView)view.findViewById(R.id.emailBox);
        password = (TextView)view.findViewById(R.id.passwordBox);
        confirmPassword = (TextView)view.findViewById(R.id.confirmPasswordBox);
        gender = (Spinner)view.findViewById(R.id.genderSpinner);
        status = (Spinner)view.findViewById(R.id.statusSpinner);
        address = (TextView)view.findViewById(R.id.addressBox);
        register = (Button)view.findViewById(R.id.registerButton);
        layout = (RelativeLayout)view.findViewById(R.id.registerLayout);

        genderAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item,gendr);
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(genderAdapter);

        statusAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,helpr);
        status.setAdapter(statusAdapter);

        password.setOnFocusChangeListener(this);
        register.setOnClickListener(this);
        layout.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.registerButton:{
                if(PasswordCheck()){

                    User u = saveUser();
                    String json = new Gson().toJson(u);

                    String url = "http://connect2help.apphb.com/api/WebSearch/PostData";

                        new PostData(this,json,url).execute();
                }
                else{
                    Toast.makeText(getActivity(), "User Registration Failed", Toast.LENGTH_SHORT).show();}
                break;
            }
            case R.id.registerLayout:{
                getActivity().getCurrentFocus().clearFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                break;
            }
        }
    }

    public User saveUser(){
        User u = new User();
        u.setUsername(username.getText().toString());
        u.setFirstName(firstname.getText().toString());
        u.setLastName(lastname.getText().toString());
        u.setEmail(email.getText().toString());
        u.setPassword(password.getText().toString());
        u.setGender(gender.getSelectedItem().toString());
        u.setUserStatus(status.getSelectedItem().toString());
        u.setAddress(address.getText().toString());
        return u;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        switch (v.getId()){
            case R.id.passwordBox:{
                if(password.getText().toString().matches(pattern)){
                    password.setBackgroundColor(getResources().getColor(R.color.good));
                    register.setClickable(true);
                }
                else{
                    password.setBackgroundColor(getResources().getColor(R.color.error));
                    register.setClickable(false);
                }
                break;
            }
            case R.id.confirmPasswordBox:{
                if(confirmPassword.getText().toString().matches(pattern)){
                    confirmPassword.setBackgroundColor(getResources().getColor(R.color.good));
                    register.setClickable(true);
                }else{
                    confirmPassword.setBackgroundColor(getResources().getColor(R.color.error));
                    register.setClickable(false);
                }
            }

        }
    }

    public boolean PasswordCheck(){

        if(register.isClickable()){
            if(password.getText().toString().equals(confirmPassword.getText().toString())){
                return true;
            }
            else{return false;}
        }
        return false;
    }


    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(getActivity(), "Please wait...", "Checking Info...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();

        Bundle b = new Bundle();
        b.putSerializable("Data", result);
        RegisterStudentLayout std = new RegisterStudentLayout();
        std.setArguments(b);

        FragmentTransaction ft =  getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame,std);
        ft.addToBackStack("fragment");
        ft.commit();
    }
}
