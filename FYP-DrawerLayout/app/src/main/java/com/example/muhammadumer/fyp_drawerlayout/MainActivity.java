package com.example.muhammadumer.fyp_drawerlayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Jobs.JobActivity;
import com.example.muhammadumer.fyp_drawerlayout.Community.Post;
import com.example.muhammadumer.fyp_drawerlayout.Members_Area.LoginLayout;
import com.example.muhammadumer.fyp_drawerlayout.Members_Area.RegisterLayout;
import com.example.muhammadumer.fyp_drawerlayout.Search.SearchResult;
import com.example.muhammadumer.fyp_drawerlayout.University.UniversityLayout;

import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements View.OnClickListener,AdapterView.OnItemClickListener, TextView.OnEditorActionListener {

    private ImageButton menuBtn;
    private ListView menuList;
    private ListView optionMenuList;
    private ImageButton searchBtn;
    private ImageButton title;

    private DrawerLayout drawerLayout;
    private EditText etSearch;
    private ImageButton dots;

    private ArrayList<String> menu;
    private ArrayList<String> menu1;
    private ArrayList<String> afterLoginMenu;

    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapter1;
    private ArrayAdapter<String> afterLoginAdapter;

    private FragmentTransaction ft;

    private RelativeLayout mainImage;

    Boolean check ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.drawer_layout);
//
//        menuBtn = (ImageButton)findViewById(R.id.drawerImage);
//        menuList = (ListView)findViewById(R.id.drawer_left);
//        drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);
//        etSearch = (EditText)findViewById(R.id.etSearch);
//        searchBtn = (ImageButton)findViewById(R.id.searchBtn);
//        title= (ImageButton)findViewById(R.id.title);
//        dots = (ImageButton)findViewById(R.id.dots);
//        optionMenuList = (ListView)findViewById(R.id.drawer_right);

        mainImage = (RelativeLayout)findViewById(R.id.bottom);


        initializeMembers();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

//        switch (v.getId()){
//            case R.id.drawerImage:
//                drawerLayout.openDrawer(GravityCompat.START);
//                break;
//            case R.id.searchBtn:
//                topBarViews();
//                break;
//            case R.id.dots:
//                drawerLayout.openDrawer(GravityCompat.END);
//                break;
//            default:
//                break;
//        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String item = parent.getItemAtPosition(position).toString().trim();
            switch (item){
                case "Home": {
                    mainImage.setVisibility(View.VISIBLE);
                    Intent i = new Intent(this,MainActivity.class);
                    startActivity(i);
                    break;
                }
                case "Jobs": {
                    mainImage.setVisibility(View.VISIBLE);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, new JobActivity());
                    ft.addToBackStack("fragment");
                    ft.commit();
                    break;
                }
                case "Education":{
                    mainImage.setVisibility(View.VISIBLE);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, new UniversityLayout());
                    ft.addToBackStack("fragment");
                    ft.commit();
                    break;
                }
                case "Community":{
                    Intent i = new Intent(this,Post.class);
                    startActivity(i);
                    break;
                }
                case "Chat":{
                    Intent i = new Intent(this,chat.MainActivity.class);
                    startActivity(i);
                    break;
                }
                case "Register":{
                    mainImage.setVisibility(View.GONE);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, new RegisterLayout());
                    ft.addToBackStack("fragment");
                    ft.commit();
                    check = true;
                    break;
                }
                case "Sign In":{
                    mainImage.setVisibility(View.GONE);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, new LoginLayout());
                    ft.addToBackStack("fragment");
                    ft.commit();
                    break;
                }
                case "Log Out":{
                    SharedPreferences preferences = getSharedPreferences("user_login_detail", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("name", "");
                    editor.commit();
                    Toast.makeText(this, "Good Bye", Toast.LENGTH_SHORT).show();
                    optionMenuList.setAdapter(adapter1);
                    break;
                }
            }
        drawerLayout.closeDrawer(GravityCompat.END);
        drawerLayout.closeDrawer(GravityCompat.START);

    }

    public void topBarViews(){
        title.setVisibility(View.GONE);
        searchBtn.setVisibility(View.GONE);
        etSearch.setVisibility(View.VISIBLE);
        etSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
    }

    public void changeVisibility(){
        etSearch.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        searchBtn.setVisibility(View.VISIBLE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
    }

    private boolean checkSession(){

        SharedPreferences sharedpreferences = getSharedPreferences("user_login_detail", this.MODE_PRIVATE);
        if (sharedpreferences.getString("name", "").equals("")){
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {

           super.onBackPressed();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            mainImage.setVisibility(View.GONE);
            getCurrentFocus().clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame,new SearchResult());
            ft.addToBackStack("fragment");
            ft.commit();
            return true;
        }
        return false;
    }

    public void initializeMembers(){
        menu = new ArrayList<String>();

        menu.add("Home");
        menu.add("Jobs");
        menu.add("Education");
        menu.add("Services");
        menu.add("Community");
        menu.add("About");
        menu.add("Chat");

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu);
        menuList.setAdapter(adapter);

        menu1 = new ArrayList<String>();

        menu1.add("Settings");
        menu1.add("Register");
        menu1.add("Sign In");

        adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu1);

        if(checkSession()){
            afterLoginMenu = new ArrayList<String>();
            afterLoginMenu.add("Settings");
            afterLoginMenu.add("Profile");
            afterLoginMenu.add("Log Out");

            afterLoginAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,afterLoginMenu);
            optionMenuList.setAdapter(afterLoginAdapter);
        }else{
            optionMenuList.setAdapter(adapter1);
        }

        menuBtn.setOnClickListener(this);
        menuList.setOnItemClickListener(this);
        searchBtn.setOnClickListener(this);
        dots.setOnClickListener(this);
        optionMenuList.setOnItemClickListener(this);

        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    changeVisibility();
                }
            }
        });

        etSearch.setOnEditorActionListener(this);
        check = false;
    }
}
