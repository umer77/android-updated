package com.example.muhammadumer.fyp_drawerlayout.Community;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class Post implements Serializable {

    private String name;
    private String questionTitle;
    private String Description;
    private ArrayList<Comment> Comments;
    private ArrayList<String> tags;
    private String UserId;
    private String PostId;
    private String Likes;
    private String Date;
    private String time;

    public void setName(String name) {
        this.name = name;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setComments(Comment comments) {
        this.Comments.add(comments);
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public void setPostId(String postId) {
        PostId = postId;
    }

    public void setLikes(String likes) {
        Likes = likes;
    }

    public void setDate(String date) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            java.util.Date d = sdf.parse(date);
            this.Date = sdf.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public String getDescription() {
        return Description;
    }

    public ArrayList<Comment> getComments() {
        return Comments;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public String getUserId() {
        return UserId;
    }

    public String getPostId() {
        return PostId;
    }

    public String getLikes() {
        return Likes;
    }

    public String getDate() {
        return Date;
    }

    public String getTime() {
        return time;
    }

    public Post() {
        this.Comments = new ArrayList<>();
        this.tags = new ArrayList<>();
    }
}
