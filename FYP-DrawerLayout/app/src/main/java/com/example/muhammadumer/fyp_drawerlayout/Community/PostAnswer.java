package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by MuhammadUmer on 3/9/2016.
 */
public class PostAnswer extends Activity implements View.OnClickListener,TaskListener {

    private TextView submit;
    private ImageView backIcon;
    EditText answer;
    ArrayList<Comment> answers;
    Post c;

    private SharedPreferences sharedPreferences;

    ProgressDialog load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.post_answer);

        submit = (TextView)findViewById(R.id.submit);
        backIcon = (ImageView)findViewById(R.id.backIcon);
        answer =(EditText)findViewById(R.id.answer);
        c = (Post)getIntent().getSerializableExtra("Data");
        answers = c.getComments();

        sharedPreferences = getSharedPreferences("user_login_detail", this.MODE_PRIVATE);

        submit.setOnClickListener(this);
        backIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                CollectInfo();
                break;
            case R.id.backIcon:{
                Intent it  = new Intent(this,PostDetailLayout.class);
                it.putExtra("Data",c);
                startActivity(it);
                this.finish();
                break;
            }
        }
    }


    public void CollectInfo(){

        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Comment comment = new Comment();
        comment.setDescription(answer.getText().toString());
        comment.setUserName(sharedPreferences.getString("name","").toString().trim());
        comment.setPostId(c.getPostId());
        comment.setDate(sdf.format(d).toString());

        String json = new Gson().toJson(comment);
        String url = "http://connect2help.apphb.com/api/WebSearch/PostCommunityAnswer";

        c.setComments(comment);

        new PostData(this,json,url).execute();

    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(this, "Please wait...", "Submitting your answer...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        Intent i  = new Intent(this,PostDetailLayout.class);
        i.putExtra("Data",c);
        startActivity(i);
        this.finish();
    }
}
