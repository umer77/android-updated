package com.example.muhammadumer.fyp_drawerlayout.University;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.Search.Degree;

/**
 * Created by MuhammadUmer on 2/16/2016.
 */
public class UniversityDetail extends Fragment implements View.OnClickListener {

    RelativeLayout layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.uni_details, container, false);

        layout = (RelativeLayout)view.findViewById(R.id.layout);
        layout.setOnClickListener(this);

        Bundle b = getArguments();

        Degree d = (Degree)b.getSerializable("Data");

        TextView name =(TextView)view.findViewById(R.id.university);
        name.setText(d.getUniversityName());

        TextView degree =(TextView)view.findViewById(R.id.uniDegree);
        degree.setText(d.getDegreeName());

        TextView loc = (TextView)view.findViewById(R.id.uniLocation);
        loc.setText("Lahore");

//        TextView desc = (TextView)view.findViewById(R.id.uniCriteria);
//        desc.setText(u.getCriteria());
//
//        TextView date = (TextView)view.findViewById(R.id.uniDate);
//        date.setText(u.getDeadline());
//
//        TextView scholarship = (TextView)view.findViewById(R.id.uniScholarship);
//        scholarship.setText(u.getScholarship());
//
//        TextView fee= (TextView)view.findViewById(R.id.uniFee);
//        fee.setText(u.getFees());

        return view;

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.layout:{
                getActivity().getCurrentFocus().clearFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                break;
            }
        }
    }
}
