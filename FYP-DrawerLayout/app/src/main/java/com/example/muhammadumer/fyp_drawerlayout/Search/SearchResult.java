package com.example.muhammadumer.fyp_drawerlayout.Search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.muhammadumer.fyp_drawerlayout.PagerAdapter;
import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 3/5/2016.
 */
public class SearchResult extends Fragment {

    private ViewPager pager;
    private PagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.after_search_view_pager, container, false);

        pager = (ViewPager)view.findViewById(R.id.pager);

        initializePaging();
        return view;
    }

    private void initializePaging() {

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(Fragment.instantiate(getActivity(), SearchResult_Education.class.getName()));
        fragments.add(Fragment.instantiate(getActivity(), SearchResult_Jobs.class.getName()));
        adapter = new PagerAdapter(getChildFragmentManager(),fragments);

        pager.setAdapter(adapter);
    }
}