package com.example.muhammadumer.fyp_drawerlayout.Members_Area;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.Student;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;

/**
 * Created by MuhammadUmer on 4/22/2016.
 */
public class RegisterStudentLayout extends Fragment implements TaskListener, View.OnClickListener {

    EditText university;
    EditText department;
    EditText degree;
    Spinner statusSpinner;
    Button register;

    int userId;

    TextView missingInfo;

    String[]status = {"Helper","Seeker"};

    ArrayAdapter<String> statusAdapter;

    RelativeLayout layout;

    ProgressDialog load;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.register_student_layout, container, false);

        Bundle b = getArguments();
        userId = Integer.parseInt((String) b.getSerializable("Data"));

        university = (EditText)view.findViewById(R.id.universityBox);
        department = (EditText)view.findViewById(R.id.departmentBox);
        degree = (EditText)view.findViewById(R.id.degreeBox);
        statusSpinner = (Spinner)view.findViewById(R.id.statusSpinner);
        register = (Button)view.findViewById(R.id.registerButton);
        missingInfo = (TextView)view.findViewById(R.id.missingInfo);
        layout = (RelativeLayout)view.findViewById(R.id.layout);

        statusAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,status);

        statusSpinner.setAdapter(statusAdapter);

        register.setOnClickListener(this);
        layout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.registerButton: {
                if (checkInfo()) {
                    Student s = saveStudent();

                    String stdObject = new Gson().toJson(s);

                    String url = "http://connect2help.apphb.com/api/WebSearch/PostStudentData";

                    new PostData(this, stdObject, url).execute();
                }
                break;
            }
            case R.id.layout:{
                getActivity().getCurrentFocus().clearFocus();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                break;
            }
        }

    }

    public boolean checkInfo(){

        Student s = saveStudent();

        if(s.getUniversity().isEmpty() || s.getDepartment().isEmpty() || s.getDegree().isEmpty() ){

            missingInfo.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    public Student saveStudent(){

        Student s = new Student();
        s.setUserId(userId);
        s.setUniversity(university.getText().toString().trim());
        s.setDepartment(department.getText().toString().trim());
        s.setDegree(degree.getText().toString().trim());
        s.setStudentStatus(statusSpinner.getSelectedItem().toString());

        return s;
    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(getActivity(), "Please wait...", "Checking Info...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();
        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
    }
}
