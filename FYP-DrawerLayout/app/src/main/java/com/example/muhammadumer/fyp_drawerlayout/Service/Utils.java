package com.example.muhammadumer.fyp_drawerlayout.Service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.muhammadumer.fyp_drawerlayout.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;

/**
 * Created by MuhammadUmer on 3/27/2016.
 */
public class Utils {

    public User getInformation(String end){
        String json;
        User result;
        json = NetworkUtils.getJSONFromAPI(end);
        Log.i("Result", json);
        result = parseJson(json);

        return result;
    }

    private User parseJson(String json){
        try {
            User person = new User();

            //   JSONObject jsonObj = new JSONObject(json);
            //  JSONArray array = jsonObj.getJSONArray("results");

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            //JSONArray array = new JSONArray(json);

            // JSONObject objArray = array.getJSONObject(0);

            JSONObject obj = new JSONObject(json);

            /*Gson gson = new Gson();
            person = gson.fromJson(obj.toString(), User.class);*/

            person.setEmail(obj.getString("email"));
            person.setUsername(obj.getString("username"));
            person.setFirstName(obj.getString("firstName"));
            person.setLastName(obj.getString("lastName"));
            person.setPassword(obj.getString("password"));
            person.setAddress(obj.getString("address"));
            person.setGender(obj.getString("gender"));
            person.setUserId(Integer.parseInt(obj.getString("userId")));
            //    Date data = new Date(obj.getString("dob"));
           // person.setDOB(obj.getString("dob"));

            return person;
        }catch (JSONException e){
            e.printStackTrace();
             User person = null;
            return person;
        }
    }

    private Bitmap downloadImage(String url) {
        try{
            URL address;
            InputStream inputStream;
            Bitmap image; address = new URL(url);
            inputStream = address.openStream();
            image = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return image;
        }catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}