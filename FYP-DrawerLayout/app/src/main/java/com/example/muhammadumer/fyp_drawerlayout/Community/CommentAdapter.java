package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class CommentAdapter extends ArrayAdapter<Comment> {


    public CommentAdapter(Context context, ArrayList<Comment> comm) {
        super(context, R.layout.community_questions_details_row_layout,comm);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.community_questions_details_row_layout, parent, false);

        Comment answer = getItem(position);

        TextView ans = (TextView)view.findViewById(R.id.answer);
        ans.setText(answer.getDescription());

        TextView name = (TextView)view.findViewById(R.id.name);
        name.setText(answer.getUserName());

        TextView date = (TextView)view.findViewById(R.id.date);
        date.setText(answer.getDate());

        return view;
    }
}
