package com.example.muhammadumer.fyp_drawerlayout.Search;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.muhammadumer.fyp_drawerlayout.Jobs.JobAdapter;
import com.example.muhammadumer.fyp_drawerlayout.Jobs.JobDetail;
import com.example.muhammadumer.fyp_drawerlayout.Jobs.Jobs;
import com.example.muhammadumer.fyp_drawerlayout.R;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 3/5/2016.
 */
public class SearchResult_Jobs extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {

    private ListView jobsList;
    private ArrayList<Jobs> jobsArrayList;
    private ArrayAdapter<Jobs> jobsArrayAdapter;

    private RelativeLayout layout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.after_search_jobs_result,container,false);

        layout = (RelativeLayout)view.findViewById(R.id.layout);
        layout.setOnClickListener(this);

        jobsList = (ListView)view.findViewById(R.id.jobsList);
        jobsArrayList = new ArrayList<Jobs>();

        Jobs j = new Jobs("Software Engineer","Lahore,Punjab Pakistan","Hello How are you? I am good.","20/02/2016");
        jobsArrayList.add(j);
        jobsArrayList.add(j);
        jobsArrayList.add(j);
        jobsArrayList.add(j);

        jobsArrayAdapter = new JobAdapter(getActivity(),jobsArrayList);
        jobsList.setAdapter(jobsArrayAdapter);
        jobsList.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle b = new Bundle();
        b.putSerializable("Data", jobsArrayList.get(position));
        JobDetail jb = new JobDetail();
        jb.setArguments(b);

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame,jb);
        ft.addToBackStack("fragment");
        ft.commit();

        hideKeyboard();
    }

    private void hideKeyboard(){
        getActivity().getCurrentFocus().clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout:{
                hideKeyboard();
            }
        }
    }
}
