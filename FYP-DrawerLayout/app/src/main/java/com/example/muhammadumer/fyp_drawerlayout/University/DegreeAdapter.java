package com.example.muhammadumer.fyp_drawerlayout.University;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.Search.Degree;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 2/16/2016.
 */
public class DegreeAdapter extends ArrayAdapter<Degree> {


    public DegreeAdapter(Context context, ArrayList<Degree> degrees) {
        super(context, R.layout.uni_row_layout,degrees);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.uni_row_layout, parent, false);


        Degree d = getItem(position);

        TextView title = (TextView)view.findViewById(R.id.jobTitle);

        title.setText(d.getDegreeName());

        TextView loc = (TextView)view.findViewById(R.id.jobLocation);
        loc.setText(d.getDegreeType());

        TextView desc = (TextView)view.findViewById(R.id.jobDescription);
        desc.setText(d.getUniversityName());
//
//        TextView date = (TextView)view.findViewById(R.id.jobDate);
//        date.setText(u.getDeadline());

        ImageView i = (ImageView)view.findViewById(R.id.image);
        i.setImageResource(R.drawable.books);

        return view;
    }
}
