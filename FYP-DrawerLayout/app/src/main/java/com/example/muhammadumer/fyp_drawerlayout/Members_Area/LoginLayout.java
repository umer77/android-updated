package com.example.muhammadumer.fyp_drawerlayout.Members_Area;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Drawer.DrawerActivity;
import com.example.muhammadumer.fyp_drawerlayout.PasswordRecoveryLayout;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.Service.Utils;
import com.example.muhammadumer.fyp_drawerlayout.User;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 2/19/2016.
 */
public class LoginLayout extends Fragment implements View.OnClickListener {

    EditText email;
    EditText password;
    TextView forgotPassword;
    Button login;
    RelativeLayout layout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.login_layout,container,false);
        email = (EditText)view.findViewById(R.id.emailBox);
        password = (EditText)view.findViewById(R.id.passwordBox);
        forgotPassword = (TextView)view.findViewById(R.id.forgotPassword);
        login = (Button)view.findViewById(R.id.loginButton);
        layout = (RelativeLayout)view.findViewById(R.id.loginLayout);

        login.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);
        layout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.forgotPassword:{
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame,new PasswordRecoveryLayout());
                ft.addToBackStack("fragment");
                ft.commit();
                break;
            }
            case R.id.loginButton:{
                if(CheckLogin()){
                   // Toast.makeText(getActivity(),"Logged in Successfully",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(), "Kindly provide your details to continue", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.loginLayout:{
                getActivity().getCurrentFocus().clearFocus();
               /* layout.setFocusable(true);
                layout.requestFocus();
                email.clearFocus();
                password.clearFocus();*/
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            //    imm.hideSoftInputFromWindow(email.getWindowToken(), 0);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
           //     imm.hideSoftInputFromWindow(password.getWindowToken(), 0);

                break;
            }
        }
    }

    private boolean CheckLogin(){

        ArrayList<User> users = new ArrayList<User>();

        if(email.getText().toString().trim() != null && password.getText().toString().trim()!=null) {

            String e = email.getText().toString().trim();
            String p = password.getText().toString().trim();

            new GetJson(e, p).execute();
            return true;
        }

        return false;
    }

    private void setSession(User user){

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user_login_detail", getActivity().MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("name", user.getUsername());
        editor.putString("userid", Integer.toString(user.getUserId()));
        editor.putString("email", user.getEmail());
        editor.commit();
    }

    private void mainMenu(){
        Intent i = new Intent(getActivity(),DrawerActivity.class);
        startActivity(i);
    }


    private class GetJson extends AsyncTask<Void, Void, User> {

        private ProgressDialog load;
        private String email;
        private String password;

        public GetJson(String email,String password)
        {
            this.email = email;
            this.password = password;
        }

        @Override
        protected void onPreExecute(){
            load = ProgressDialog.show(getActivity(), "Please wait...", "Getting Login details...");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected User doInBackground(Void... params) {
            Utils util = new Utils();

            String url = "http://connect2help.apphb.com/api/websearch/getLoginUser/"+this.email + "/"+this.password;

            User u = util.getInformation(url);
            return u;
        }

        @Override
        protected void onPostExecute(User person){

            load.dismiss();
            if(person!= null) {
                setSession(person);
                Toast.makeText(getActivity(), "Logged in Successfully", Toast.LENGTH_SHORT).show();
                mainMenu();
            }else{
                Toast.makeText(getActivity(), "Wrong Password.Logged in Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
