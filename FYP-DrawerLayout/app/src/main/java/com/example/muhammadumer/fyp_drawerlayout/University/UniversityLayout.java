package com.example.muhammadumer.fyp_drawerlayout.University;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.Search.Degree;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 2/16/2016.
 */
public class UniversityLayout extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener,TaskListener {

    ListView uniList;
    ArrayList<Degree> degrees;
    ArrayAdapter<Degree> degreeArrayAdapter;

    private RelativeLayout layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.university_layout,container,false);

        uniList = (ListView)view.findViewById(R.id.universityList);
        layout = (RelativeLayout)view.findViewById(R.id.layout1);

        layout.setOnClickListener(this);



        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle b = new Bundle();
        b.putSerializable("Data", degrees.get(position));
        UniversityDetail u = new UniversityDetail();
        u.setArguments(b);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frame,u);
        ft.addToBackStack("fragment");
        ft.commit();
        hideKeyboard();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout1:{
                hideKeyboard();
                break;
            }
        }
    }

    private void hideKeyboard(){
        getActivity().getCurrentFocus().clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
    }

    @Override
    public void onTaskStarted() {

    }

    @Override
    public void onTaskFinished(String result) {

    }
}
