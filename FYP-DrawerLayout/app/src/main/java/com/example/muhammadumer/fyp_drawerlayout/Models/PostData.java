package com.example.muhammadumer.fyp_drawerlayout.Models;

import android.os.AsyncTask;

import com.example.muhammadumer.fyp_drawerlayout.Service.NetworkUtils;

/**
 * Created by MuhammadUmer on 4/23/2016.
 */
public class PostData extends AsyncTask<Void,Void,String> {

    private final TaskListener listener;

    String obj;
    String url;

    public PostData(TaskListener listener,String object,String url){
        this.obj = object;
        this.url = url;
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(String s) {
        listener.onTaskFinished(s);
    }

    @Override
    protected String doInBackground(Void... voids) {
        String result = NetworkUtils.excutePost(url, obj);

        return result;
    }

    @Override
    protected void onPreExecute() {
        listener.onTaskStarted();
    }
}
