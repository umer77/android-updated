package com.example.muhammadumer.fyp_drawerlayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 3/5/2016.
 */
public class PagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragments;

    public PagerAdapter(FragmentManager fm,ArrayList<Fragment> frags) {
        super(fm);
        this.fragments = frags;

    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch(position){
            case 0:
                return "Education";
            case 1:
                return "Jobs";

        }
        return "";
    }
}
