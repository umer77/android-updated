package com.example.muhammadumer.fyp_drawerlayout.Community;

import java.io.Serializable;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class Comment implements Serializable {

    private String UserName;
    private String Description;
    private String Date;
    private String PostId;
    private int UserId;

    public String getUserName() {
        return UserName;
    }

    public String getDescription() {
        return Description;
    }

    public String getDate() {
        return Date;
    }

    public String getPostId() {
        return PostId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setPostId(String postId) {
        PostId = postId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }
}
