package com.example.muhammadumer.fyp_drawerlayout.Jobs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.muhammadumer.fyp_drawerlayout.R;

/**
 * Created by MuhammadUmer on 2/15/2016.
 */
public class JobDetail extends android.support.v4.app.Fragment implements View.OnClickListener {

    RelativeLayout layout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.job_details, container, false);

        Bundle b = getArguments();
        Jobs j = (Jobs) b.getSerializable("Data");

        TextView title =(TextView)view.findViewById(R.id.jobProvider);
        title.setText("Mind-Games Studios");

        TextView position =(TextView)view.findViewById(R.id.jobPosition);
        position.setText(j.getTitle());

        TextView loc = (TextView)view.findViewById(R.id.jobLocation);
        loc.setText(j.getLocation());

        TextView desc = (TextView)view.findViewById(R.id.jobDescription);
        desc.setText(j.getDescription());
        desc.setEditableFactory(Editable.Factory.getInstance());

        TextView date = (TextView)view.findViewById(R.id.jobDate);
        date.setText(j.getApplyDate());

        layout = (RelativeLayout)view.findViewById(R.id.layout);
        layout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout:{
                getActivity().getCurrentFocus().clearFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),0);
                break;
            }
        }
    }
}
