package com.example.muhammadumer.fyp_drawerlayout.Jobs;

import java.io.Serializable;

/**
 * Created by MuhammadUmer on 2/14/2016.
 */
public class Jobs implements Serializable {

    private String title;
    private String location;
    private String description;
    private String applyDate;

    public void setApplyDate(String applyDate) {
        this.applyDate = applyDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public String getApplyDate() {
        return applyDate;
    }

    public Jobs(String title, String location, String description, String applyDate) {
        this.setTitle(title);
        this.setLocation(location);
        this.setDescription(description);
        this.setApplyDate(applyDate);
    }
}
