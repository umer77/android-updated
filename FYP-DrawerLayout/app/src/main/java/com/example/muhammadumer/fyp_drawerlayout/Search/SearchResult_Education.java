package com.example.muhammadumer.fyp_drawerlayout.Search;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.University.DegreeAdapter;
import com.example.muhammadumer.fyp_drawerlayout.University.UniversityDetail;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 3/5/2016.
 */
public class SearchResult_Education extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener,TaskListener {

    private ListView uniList;
    private ArrayList<Degree> degrees;
    private ArrayAdapter<Degree> degreeArrayAdapter;

    private RelativeLayout layout;
    private ProgressDialog load;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.after_search_education_result, container, false);
        uniList = (ListView)view.findViewById(R.id.universityList);

        layout = (RelativeLayout)view.findViewById(R.id.layout);
        layout.setOnClickListener(this);
        Bundle b = getArguments();
        degrees = (ArrayList<Degree>)b.getSerializable("Data");

        degreeArrayAdapter = new DegreeAdapter(getActivity(),degrees);
        uniList.setAdapter(degreeArrayAdapter);
        uniList.setOnItemClickListener( this);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String url = "http://connect2help.apphb.com/api/WebSearch/PostDegreeDetail";

        new PostData((TaskListener) getActivity(),Integer.toString(degrees.get(position).getDegreeId()),url).execute();

    }

    private void hideKeyboard(){
        getActivity().getCurrentFocus().clearFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout:{
                hideKeyboard();
            }
        }
    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(getActivity(), "Searching...", "It may take a while...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();

        Bundle b = new Bundle();
        b.putSerializable("Data", "");
        UniversityDetail u = new UniversityDetail();
        u.setArguments(b);

        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame,u);
        ft.addToBackStack("fragment");
        ft.commit();

        hideKeyboard();
    }
}
