package com.example.muhammadumer.fyp_drawerlayout.University;

import com.example.muhammadumer.fyp_drawerlayout.Search.Degree;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 2/16/2016.
 */
public class University implements Serializable {

    private String name;
    private String criteria;
    private String fees;
    private String scholarship;
    private String deadline;
    private String location;
    private ArrayList<Degree> degrees;

    public University(String name, String criteria, String fees, String scholarship, String deadline,String location) {
        this.setName(name);
        this.setCriteria(criteria);
        this.setFees(fees);
        this.setScholarship(scholarship);
        this.setDeadline(deadline);
        this.setLocation(location);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public void setScholarship(String scholarship) {
        this.scholarship = scholarship;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setDegrees(Degree degree){
        this.degrees.add(degree);
    }

    public String getName() {return name;}

    public String getCriteria() {
        return criteria;
    }

    public String getFees() {
        return fees;
    }

    public String getScholarship() {
        return scholarship;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getLocation() {return location;}


    public ArrayList<Degree> getDegrees() {
        return degrees;
    }

    public University() {
        degrees = new ArrayList<>();
    }
}
