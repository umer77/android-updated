package com.example.muhammadumer.fyp_drawerlayout.Models;

/**
 * Created by MuhammadUmer on 4/23/2016.
 */
public class Student {

    private String University;
    private String Department;
    private String Degree;
    private String StudentStatus;
    private int UserId;

    public String getUniversity() {
        return University;
    }

    public String getDepartment() {
        return Department;
    }

    public String getDegree() {
        return Degree;
    }

    public String getStudentStatus() {
        return StudentStatus;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUniversity(String university) {
        University = university;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    public void setDegree(String degree) {
        Degree = degree;
    }

    public void setStudentStatus(String studentStatus) {
        StudentStatus = studentStatus;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }
}
