package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by MuhammadUmer on 3/9/2016.
 */
public class PostQuestion extends Activity implements View.OnClickListener ,TaskListener{

    private TextView submit;
    private ImageView backIcon;
    private EditText question;
    private SharedPreferences sharedpreferences;
    private ProgressDialog load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.post_question);

        submit = (TextView)findViewById(R.id.submit);
        backIcon = (ImageView)findViewById(R.id.backIcon);
        question = (EditText)findViewById(R.id.Description);
        sharedpreferences = getSharedPreferences("user_login_detail", this.MODE_PRIVATE);

        submit.setOnClickListener(this);
        backIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                checkUser();
                break;
            case R.id.backIcon:{
                Intent it  = new Intent(this,PostLayout.class);
                startActivity(it);
                this.finish();
                break;
            }
        }
    }

    public void checkUser(){

        if (sharedpreferences.getString("name", "").equals("")){
            Toast.makeText(this, "You must log in Continue", Toast.LENGTH_SHORT).show();
        }else {
            CollectInfo();
        }
    }

    public void CollectInfo(){

        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Comment c = new Comment();
        c.setDescription(question.getText().toString());
       c.setUserName(sharedpreferences.getString("name", "").toString().trim());
        c.setUserId(Integer.parseInt(sharedpreferences.getString("userid", "").toString().trim()));
        c.setDate(sdf.format(d).toString());

        String json = new Gson().toJson(c);

        String url = "http://connect2help.apphb.com/api/WebSearch/PostCommunityQuestion";

        new PostData(this,json,url).execute();

    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(this, "Please wait...", "Submitting your Query...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
        Intent i  = new Intent(this,PostLayout.class);
        startActivity(i);
        this.finish();
    }
}
