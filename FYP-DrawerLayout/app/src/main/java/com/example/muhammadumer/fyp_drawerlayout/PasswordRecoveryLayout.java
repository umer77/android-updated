package com.example.muhammadumer.fyp_drawerlayout;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Password_Recovery.GMailSender;

import java.util.Random;

/**
 * Created by MuhammadUmer on 2/19/2016.
 */
public class PasswordRecoveryLayout extends Fragment implements View.OnClickListener {

    Button b;
    private EditText email;
    private GMailSender sender;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.password_recovery_layout,container,false);

        sender = new GMailSender("bcsf12m535@pucit.edu.pk", "Atlantausa@77");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.
                Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        b =(Button)view.findViewById(R.id.continueButton);
        email = (EditText)view.findViewById(R.id.emailBox);
        b.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.continueButton:{
                new MyAsyncClass(email.getText().toString().trim()).execute();
            }
        }
    }

    class MyAsyncClass extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        String email;

        public MyAsyncClass(String e){
            email = e;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... mApi) {
            try {
                int num;
                Random r  =  new Random();
                num = 100000 + r.nextInt(900000);
                String code = String.valueOf(num);
                // Add subject, Body, your mail Id, and receiver mail Id.
                sender.sendMail("Subject", "Your Activation code is "+code, "itbizz519@gmail.com", email);

            }

            catch (Exception ex) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.cancel();
            Toast.makeText(getActivity(), "An activation code has been sent to you email", Toast.LENGTH_SHORT).show();
        }
    }
}
