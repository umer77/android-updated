package com.example.muhammadumer.fyp_drawerlayout.Drawer;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadumer.fyp_drawerlayout.Community.PostLayout;
import com.example.muhammadumer.fyp_drawerlayout.Jobs.JobActivity;
import com.example.muhammadumer.fyp_drawerlayout.Members_Area.LoginLayout;
import com.example.muhammadumer.fyp_drawerlayout.Members_Area.RegisterLayout;
import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;
import com.example.muhammadumer.fyp_drawerlayout.Search.Degree;
import com.example.muhammadumer.fyp_drawerlayout.Search.SearchResult_Education;
import com.example.muhammadumer.fyp_drawerlayout.University.UniversityLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import chat.MainActivity;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SearchView.OnQueryTextListener, TaskListener {

    private FragmentTransaction ft;
    private NavigationView navigationView;
    private RelativeLayout mainImage;
    private TextView username;
    private TextView userEmail;
    private ProgressDialog load;

    private String name;

    private MenuItem searchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mainImage = (RelativeLayout)findViewById(R.id.bottom);

        View headerLayout = navigationView.getHeaderView(0);
        username = (TextView)headerLayout.findViewById(R.id.loginUserName);
        userEmail = (TextView)headerLayout.findViewById(R.id.loginUserEmail);

        username.setOnClickListener(this);
        checkSession();

        /*Button b = (Button)findViewById(R.id.chat);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), chat.MainActivity.class);
                startActivity(i);

            }
        });*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);

        searchMenuItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            this.finish();
            Intent i = new Intent(this,DrawerActivity.class);
            startActivity(i);
        } else if (id == R.id.education) {
            mainImage.setVisibility(View.GONE);
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame, new UniversityLayout());
            ft.addToBackStack("fragment");
            ft.commit();
        } else if (id == R.id.job) {
            mainImage.setVisibility(View.GONE);
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame, new JobActivity());
            ft.addToBackStack("fragment");
            ft.commit();
        } else if (id == R.id.community) {
            mainImage.setVisibility(View.GONE);
            Intent i = new Intent(this,PostLayout.class);
            startActivity(i);
        }
        else if (id == R.id.chat) {
            mainImage.setVisibility(View.GONE);
            SharedPreferences sharedpreferences = getSharedPreferences("user_login_detail", this.MODE_PRIVATE);
            name = sharedpreferences.getString("name", "");
            Intent i = new Intent(this, chat.MainActivity.class);
            i.putExtra("x",name);
            startActivity(i);
        }
        else if (id == R.id.register) {
            mainImage.setVisibility(View.GONE);
            ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame, new RegisterLayout());
            ft.addToBackStack("fragment");
            ft.commit();
        }else if (id == R.id.logout) {
            SharedPreferences preferences = getSharedPreferences("user_login_detail", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("name", "");
            editor.commit();
            Toast.makeText(this, "Good Bye", Toast.LENGTH_SHORT).show();
            checkSession();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void checkSession(){

        SharedPreferences sharedpreferences = getSharedPreferences("user_login_detail", this.MODE_PRIVATE);

        if (sharedpreferences.getString("name", "").equals("")){
            navigationView.getMenu().findItem(R.id.logout).setVisible(false);
            navigationView.getMenu().findItem(R.id.chat).setVisible(false);
            username.setText("SIGN IN");
            userEmail.setVisibility(View.GONE);
        }else{
            navigationView.getMenu().findItem(R.id.logout).setVisible(true);
            navigationView.getMenu().findItem(R.id.chat).setVisible(true);
            username.setText(sharedpreferences.getString("name","").toString().trim());
            userEmail.setText(sharedpreferences.getString("email","").toString().trim());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginUserName:{
                TextView tv = (TextView)v;
                if(tv.getText().toString().trim().equals("SIGN IN")){
                    mainImage.setVisibility(View.GONE);
                    ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.frame, new LoginLayout());
                    ft.addToBackStack("fragment");
                    ft.commit();
                }
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        String url = "http://connect2help.apphb.com/api/WebSearch/PostSearchResult";

        new PostData(this,query,url).execute();

        Toast.makeText(this,query,Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(this, "Searching...", "It may take a while...");
    }

    @Override
    public void onTaskFinished(String result) {
        load.dismiss();
        JSONArray array;
        ArrayList<Degree> degrees = new ArrayList<>();
        JSONObject obj,obj1;
        Degree d ;
        try {
             array = new JSONArray(result);
            for(int i=0;i<array.length();i++){
                d = new Degree();
                obj = new JSONObject(array.getJSONObject(i).toString());
                obj1 = obj.getJSONObject("university");
                d.setUniversityName(obj1.getString("fullName"));
                d.setDegreeId(Integer.parseInt(obj.getString("degreeId")));
                d.setDegreeType(obj.getString("degreeType"));
                d.setDegreeName(obj.getString("fullName"));
                d.setUniversityId(obj.getString("universityId"));
                degrees.add(d);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle b = new Bundle();
        b.putSerializable("Data", degrees);
        SearchResult_Education sr = new SearchResult_Education();
        sr.setArguments(b);

        hideKeyboard();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frame,sr);
        ft.addToBackStack("fragment");
        ft.commit();
    }

    public void hideKeyboard(){
        mainImage.setVisibility(View.GONE);
        MenuItemCompat.collapseActionView(searchMenuItem);
    }
}
