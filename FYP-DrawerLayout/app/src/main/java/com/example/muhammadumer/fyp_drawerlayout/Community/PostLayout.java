package com.example.muhammadumer.fyp_drawerlayout.Community;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.muhammadumer.fyp_drawerlayout.Models.PostData;
import com.example.muhammadumer.fyp_drawerlayout.Models.TaskListener;
import com.example.muhammadumer.fyp_drawerlayout.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by MuhammadUmer on 5/17/2016.
 */
public class PostLayout extends Activity implements AdapterView.OnItemClickListener, View.OnClickListener,TaskListener {

    private ListView questionsList;
    private ArrayList<Post> communityArrayList;
    private ArrayAdapter<Post> communityArrayAdapter;
    private ArrayList<Comment> ans;

    ImageView questionIcon;
    ImageView backIcon;

    private ProgressDialog load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.community_layout);
        questionsList = (ListView)findViewById(R.id.questionsList);
        questionIcon = (ImageView)findViewById(R.id.questionicon);
        backIcon = (ImageView)findViewById(R.id.backIcon);

        communityArrayList = new ArrayList<>();
        ans = new ArrayList<>();

        GetCommunityData();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent i = new Intent(this,PostDetailLayout.class);
        i.putExtra("Data",communityArrayList.get(position));
        startActivity(i);
        this.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.questionicon:
                Intent i = new Intent(this,PostQuestion.class);
                startActivity(i);
                this.finish();
                break;
            case R.id.backIcon:
                super.onBackPressed();
                break;
        }
    }

    private void GetCommunityData(){
        String url = "http://connect2help.apphb.com/api/WebSearch/PostCommunityData";
        String data = "5";
        new PostData(this,data,url).execute();
    }

    @Override
    public void onTaskStarted() {
        load = ProgressDialog.show(this, "Please wait...", "Getting Community Data");
    }

    @Override
    public void onTaskFinished(String result)  {
        load.dismiss();

        JSONArray arr = null;
        try {
            arr = new JSONArray(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int j=0;j<arr.length();j++) {

            Post c = new Post();
            try {

                JSONObject obj = new JSONObject(arr.getJSONObject(j).toString());
                c.setDescription(obj.getString("description"));
                c.setDate(obj.getString("date"));
                c.setUserId(obj.getString("userId"));
                //c.setName(obj.getString("userName"));
                c.setPostId(obj.getString("postId"));

                JSONArray array = obj.getJSONArray("comments");

                for (int i = 0; i < array.length(); i++) {
                    JSONObject obj1 = array.getJSONObject(i);
                    Comment answers = new Comment();
                    answers.setDate(obj1.getString("date"));
                    answers.setUserName(obj1.getString("userName"));
                    answers.setDescription(obj1.getString("description"));
                    answers.setPostId(obj1.getString("postId"));
                    c.setComments(answers);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            communityArrayList.add(c);

        }
        communityArrayAdapter = new PostAdapter(this,communityArrayList);

        questionsList.setAdapter(communityArrayAdapter);
        questionsList.setOnItemClickListener(this);

        questionIcon.setOnClickListener(this);
        backIcon.setOnClickListener(this);
    }
}
